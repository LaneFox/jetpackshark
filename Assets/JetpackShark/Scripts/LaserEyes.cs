﻿using UnityEngine;
using System.Collections;

namespace JetpackShark
{
    public static class StaticHelpers
    {
        public static bool LayerMatchTest(LayerMask approvedLayers, GameObject objInQuestion)
        {
            return ((1 << objInQuestion.layer) & approvedLayers) != 0;
        }
    }

    public class LaserEyes : MonoBehaviour
    {
        public float MaxDistance = 25;
        public LayerMask HitLayer;
        public LineRenderer Lazor;

        private Vector3 _startPoint;
        private Vector3 _endPoint;

        public GameObject ExplosionPrefab;

        void Update()
        {
            _startPoint = transform.position;
            RaycastHit hit;
            Physics.Raycast(_startPoint, transform.forward, out hit, MaxDistance, HitLayer);

            if (hit.transform == null) _endPoint = _startPoint + (transform.forward*MaxDistance);
            else
            {
                _endPoint = hit.point;

                if (StaticHelpers.LayerMatchTest(HitLayer, hit.collider.gameObject))
                {
                    Destroy(hit.collider.gameObject);
                    MakeExplosionAt(hit.collider.transform.position);
                }
            }

            Lazor.SetPosition(0, _startPoint);
            Lazor.SetPosition(1, _endPoint);

        }

        void MakeExplosionAt(Vector3 position)
        {
            ScoreKeeper.AddPointsForRock();
            Instantiate(ExplosionPrefab, position, Quaternion.identity);
        }
    }
}