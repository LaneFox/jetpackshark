﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

namespace JetpackShark
{
    public class SharkControl : MonoBehaviour
    {
        public Rigidbody Rb;
        public int Fatness = 1;

        [Space]
        public float BaseSpeed = 5;
        public float CurrentSpeed = 5f;

        [Space]
        public float ControlSensitivity = 10;
        public GameObject AteCupcakeFx;
        public LayerMask CupcakeLayer;
        public LayerMask RockLayer;
        public LayerMask PowerupLayer;
        public string AxisXName = "Horizontal";
        public string AxisYName = "Vertical";
        public float GameplayRadius = 200f;

        [Space]
        public float LaserModeTime = 8;
        private bool InLaserMode = false;
        public GameObject LaserObj1;

        void OnCollisionEnter(Collision col)
        {
            if (StaticHelpers.LayerMatchTest(CupcakeLayer, col.gameObject))
            {
                ScoreKeeper.AddPointsForCupcake();
                Fatness++;
                Instantiate(AteCupcakeFx, col.transform.position, Random.rotation);
                Destroy(col.gameObject);
                return;
            }

            if (StaticHelpers.LayerMatchTest(PowerupLayer, col.gameObject))
            {
                ScoreKeeper.AddPointsForPowerup();
                StartCoroutine(LaserSuperModeOfDoom());
                return;
            }

            if (StaticHelpers.LayerMatchTest(RockLayer, col.gameObject))
            {
                Die();
            }
        }
        void Update()
        {
            if (Vector3.Distance(Vector3.zero, transform.position) > GameplayRadius) MoveToStart();
            transform.Translate(Vector3.forward * CurrentSpeed * Fatness * Time.deltaTime);
            Vector3 input = new Vector3(-CrossPlatformInputManager.GetAxis(AxisYName) * ControlSensitivity, CrossPlatformInputManager.GetAxis(AxisXName) * ControlSensitivity, 0);
            transform.rotation *= Quaternion.Euler(input);
        }

        public IEnumerator LaserSuperModeOfDoom()
        {
            if (InLaserMode) yield break;

            LaserObj1.SetActive(true);
            yield return new WaitForSeconds(LaserModeTime);
            LaserObj1.SetActive(false);
        }

        public void Die()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        void MoveToStart()
        {
            transform.position = Vector3.zero;
        }
    }
}