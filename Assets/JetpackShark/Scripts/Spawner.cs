﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace JetpackShark
{
    public class Spawner : MonoBehaviour
    {
        public List<GameObject> LiveObjects;
        public GameObject Rock;
        public GameObject Powerup;
        public GameObject Cupcake;
        public int NumOfCupcakes = 10;
        public int NumOfPowerups = 3;
        public int NumOfRocks = 30;
        public float SpawnRadius = 100;

        void Start()
        {
            SpawnRandomStuff();
        }

        public void SpawnRandomStuff()
        {
            if (NumOfCupcakes > 0 && Cupcake) Spawn(Cupcake, NumOfCupcakes);
            if (NumOfRocks > 0 && Rock) Spawn(Rock, NumOfRocks);
            if (NumOfPowerups > 0 && Powerup) Spawn(Powerup, NumOfPowerups);
        }

        private void Spawn(GameObject these, int thisMany)
        {
            for (int i = 0; i < thisMany; i++)
            {
                GameObject thing = Instantiate(these, Random.insideUnitSphere*SpawnRadius, Random.rotation) as GameObject;
                thing.transform.localScale *= i == 0 || i == 1 ? 1 : i/2;
                LiveObjects.Add(thing);
                thing.transform.SetParent(transform);
            }
        }
    }
}