﻿using UnityEngine;
using System.Collections;

namespace JetpackShark
{
    public class SpinScript : MonoBehaviour
    {
        public float RotateSpeed;

        void Update()
        {
            transform.rotation *= Quaternion.Euler(new Vector3(0, RotateSpeed, 0));
        }
    }
}