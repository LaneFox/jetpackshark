﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace JetpackShark
{
    public static class ScoreKeeper
    {
        public static int ScoreForCupcake;
        public static int ScoreForRock;
        public static int ScoreForPowerup;

        public static int CurrentScore;
        public static int TopScore
        {
            get
            {
                return PlayerPrefs.GetInt("TopScore");
            }
            set
            {
                PlayerPrefs.SetInt("TopScore", value);
                PlayerPrefs.Save();
            }
        }

        public static void AddPointsForCupcake() { AddToCurrentScore(ScoreForCupcake); }
        public static void AddPointsForRock() { AddToCurrentScore(ScoreForRock); }
        public static void AddPointsForPowerup() { AddToCurrentScore(ScoreForPowerup); }
        public static void AddToCurrentScore(int thisMuch) { CurrentScore += thisMuch; }
        public static void SaveScore() { if (CurrentScore > TopScore) TopScore = CurrentScore; }
    }
    public class Score : MonoBehaviour
    {
        public int RockPoints = 1578;
        public int CupcakePoints = 3529;
        public int PowerupPoints = 7642;

        public Text CurrentScoreText;
        public Text TopScoreText;
        public float UpdateInterval = 1;
        private float _interval;

        void Start()
        {
            ScoreKeeper.CurrentScore = 0;
            ScoreKeeper.ScoreForCupcake = CupcakePoints;
            ScoreKeeper.ScoreForRock = RockPoints;
            ScoreKeeper.ScoreForPowerup = PowerupPoints;
        }

        void Update()
        {
            _interval += Time.deltaTime;

            if (_interval > UpdateInterval)
            {
                ScoreKeeper.SaveScore();
                _interval = 0f;
                CurrentScoreText.text = ScoreKeeper.CurrentScore.ToString();
                TopScoreText.text = ScoreKeeper.TopScore.ToString();
            }
        }
    }
}