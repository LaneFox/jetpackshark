﻿using UnityEngine;
using System.Collections;

namespace JetpackShark
{
    public class Lifetimer : MonoBehaviour
    {
        public float LifeTime;
        private float _time;

        void Update()
        {
            _time += Time.deltaTime;
            if (_time > LifeTime) Destroy(gameObject);
        }
    }
}