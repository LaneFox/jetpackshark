﻿using UnityEngine;
using System.Collections;
using JetpackShark;

public class PowerUp : MonoBehaviour
{
    public float RotateSpeed;

    void Update()
    {
        transform.rotation *= Quaternion.Euler(new Vector3(0, RotateSpeed, 0));
    }

    void OnCollisionEnter(Collision col)
    {
        SharkControl shark = col.collider.GetComponent<SharkControl>();
        if (shark) StartCoroutine(shark.LaserSuperModeOfDoom());
        Destroy(gameObject);
    }
}